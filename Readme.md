Projet Pirate : Legislatives 2017 - Gestion des candidatures
===

DONE
---

- Récupérer les données du backoffice candidats.
- Récupérer les données de la carte.
- Indiquer le plus grand Id des candidats
- Lister les candidats sans entrée BO
- Lister les candidats sans Section
- Lister les sections
- Lister les candidats par section sans tuteur
- Lister les circos avec candidats.

TODO - Dev
---

- Lister les circos avec candidature complète.
- Lister les circos avec candidature complète et investiture.

TODO - Data
---
Demander aux pirates suivants d'aller s'inscrire sur https://2017.partipirate.org/

- #075-18 - Aurélie
- #092-01 - Brevignon Thomas
- #092-12 - Laure Ferry
- #092-12 - Mathieu Garrigues
- #093-06 - Ludovic Locko
- #093-07 - Elodie Queudrue
- #095-01 - Patrick Mazeirat

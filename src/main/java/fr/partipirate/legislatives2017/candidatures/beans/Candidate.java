package fr.partipirate.legislatives2017.candidatures.beans;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Candidate extends RemoteCsvBean {

	private Long id;
	private String name;
	private GENDER gender;
	private boolean candidate;
	private boolean substitute;
	private boolean representative;
	private String section;
	private boolean investi;
	private String tutor;
	private String circos;

	public Candidate(String line) {
		String[] fields = line.split(",");
		this.id             = getLong(fields, 0);
		this.name           = getString(fields, 1);
		this.gender         = getGender(fields, 2);
		this.candidate      = getBoolean(fields, 3);
		this.substitute     = getBoolean(fields, 4);
		this.representative = getBoolean(fields, 5);
		this.section        = getString(fields, 6);
		this.investi        = getBoolean(fields, 7);
		this.tutor          = getString(fields, 8);
		if (fields.length > 9) {
			this.circos = Arrays.stream(fields)
				.skip(9)
				.collect(Collectors.joining(","))
				.replaceAll("\"", "");
		}
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public GENDER getGender() {
		return gender;
	}

	public boolean isCandidate() {
		return candidate;
	}

	public boolean isSubstitute() {
		return substitute;
	}

	public boolean isRepresentative() {
		return representative;
	}

	public String getSection() {
		return section;
	}

	public boolean isInvesti() {
		return investi;
	}

	public String getTutor() {
		return tutor;
	}

	public String getCircos() {
		return circos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (candidate ? 1231 : 1237);
		result = prime * result + ((circos == null) ? 0 : circos.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (investi ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (representative ? 1231 : 1237);
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		result = prime * result + (substitute ? 1231 : 1237);
		result = prime * result + ((tutor == null) ? 0 : tutor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Candidate other = (Candidate) obj;
		if (candidate != other.candidate)
			return false;
		if (circos == null) {
			if (other.circos != null)
				return false;
		} else if (!circos.equals(other.circos))
			return false;
		if (gender != other.gender)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (investi != other.investi)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (representative != other.representative)
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		if (substitute != other.substitute)
			return false;
		if (tutor == null) {
			if (other.tutor != null)
				return false;
		} else if (!tutor.equals(other.tutor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Candidate [id=" + id + ", name=" + name + ", gender=" + gender + ", candidate=" + candidate
				+ ", substitute=" + substitute + ", representative=" + representative + ", section=" + section
				+ ", investi=" + investi + ", tutor=" + tutor + ", circos=" + circos + "]";
	}
	
	
}

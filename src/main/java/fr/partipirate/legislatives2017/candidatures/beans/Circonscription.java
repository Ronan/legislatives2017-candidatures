package fr.partipirate.legislatives2017.candidatures.beans;

public class Circonscription extends RemoteCsvBean{

	private String id;
	private String name;
	private boolean ok;
	private String candidate;
	private String substitute;
	private String representative;
	private String label;
	private String css;
	private String website;
	private String picture;
	
	public Circonscription(String line) {
		String[] fields = line.split(",");
		this.id         = getString(fields, 0);
		this.name       = getString(fields, 1);
		this.ok         = getBoolean(fields, 2);
		this.candidate  = getString(fields, 3);
		this.substitute = getString(fields, 4);
		this.label      = getString(fields, 5);
		this.css        = getString(fields, 6);
		this.website    = getString(fields, 7);
		this.picture    = getString(fields, 8);
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isOk() {
		return ok;
	}

	public String getCandidate() {
		return candidate;
	}

	public String getSubstitute() {
		return substitute;
	}

	public String getRepresentative() {
		return representative;
	}

	public String getLabel() {
		return label;
	}

	public String getCss() {
		return css;
	}

	public String getWebsite() {
		return website;
	}

	public String getPicture() {
		return picture;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((candidate == null) ? 0 : candidate.hashCode());
		result = prime * result + ((css == null) ? 0 : css.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (ok ? 1231 : 1237);
		result = prime * result + ((picture == null) ? 0 : picture.hashCode());
		result = prime * result + ((representative == null) ? 0 : representative.hashCode());
		result = prime * result + ((substitute == null) ? 0 : substitute.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circonscription other = (Circonscription) obj;
		if (candidate == null) {
			if (other.candidate != null)
				return false;
		} else if (!candidate.equals(other.candidate))
			return false;
		if (css == null) {
			if (other.css != null)
				return false;
		} else if (!css.equals(other.css))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ok != other.ok)
			return false;
		if (picture == null) {
			if (other.picture != null)
				return false;
		} else if (!picture.equals(other.picture))
			return false;
		if (representative == null) {
			if (other.representative != null)
				return false;
		} else if (!representative.equals(other.representative))
			return false;
		if (substitute == null) {
			if (other.substitute != null)
				return false;
		} else if (!substitute.equals(other.substitute))
			return false;
		if (website == null) {
			if (other.website != null)
				return false;
		} else if (!website.equals(other.website))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Circonscription [id=" + id + ", name=" + name + ", ok=" + ok + ", candidate=" + candidate
				+ ", substitute=" + substitute + ", representative=" + representative + ", label=" + label + ", css="
				+ css + ", website=" + website + ", picture=" + picture + "]";
	}


}

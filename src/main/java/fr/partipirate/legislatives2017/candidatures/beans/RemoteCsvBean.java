package fr.partipirate.legislatives2017.candidatures.beans;

public class RemoteCsvBean {

	protected String getString(String[] fields, int idx) {
		String res = null;
		if (fields.length > idx)
			res = fields[idx].replaceAll("\"", "");
		return res;
	}

	protected boolean getBoolean(String[] fields, int idx) {
		boolean res = false;
		if (fields.length > idx) {
			if (   "1".equalsIgnoreCase(fields[idx])
                || "oui".equalsIgnoreCase(fields[idx])) {
				res = true;
			}
		}
		return res;
	}
	
	protected GENDER getGender(String[] fields, int idx) {
		GENDER res = null;
		if (fields.length > idx)
			if (!"".equals(fields[idx].trim())) {
				res = GENDER.valueOf(fields[idx].toUpperCase());
			}
		return res;
	}

	protected Long getLong(String[] fields, int idx) {
		Long res = null;
		if (fields.length > idx)
			if (!"".equals(fields[idx].trim())) {
				res = Long.parseLong(fields[idx]);
			}
		return res;
	}

}

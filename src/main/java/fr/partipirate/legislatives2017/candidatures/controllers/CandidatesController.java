package fr.partipirate.legislatives2017.candidatures.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;
import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.candidatures.repositories.CirconscriptionsRepository;

@Controller
@EnableAutoConfiguration
@RequestMapping("/candidates")
public class CandidatesController {

	private CandidatesRepository candidates;
	private CirconscriptionsRepository circonscriptions;

	@Autowired
    public CandidatesController(CandidatesRepository candidates, CirconscriptionsRepository circonscriptions) {
		this.candidates = candidates;
		this.circonscriptions = circonscriptions;
	}

	@RequestMapping(value="/not-registered", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> notRegistered() {
		return circonscriptions.allCandidates()
				.filter(candidate -> candidates.getByName(candidate.getName()) == null)
				.collect(Collectors.toList());
	}

	@RequestMapping(value="/without-section", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> withoutSection() {
		return candidates.all()
				.filter(c -> c.getSection() != null)
				.filter(candidate -> "".equals(candidate.getSection().trim()))
				.collect(Collectors.toList());
	}
}

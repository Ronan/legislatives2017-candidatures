package fr.partipirate.legislatives2017.candidatures.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;
import fr.partipirate.legislatives2017.candidatures.beans.Circonscription;
import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.candidatures.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.candidatures.services.DataNormalizer;

@Controller
@EnableAutoConfiguration
@RequestMapping("/circos")
public class CirconscriptionController {

	private CandidatesRepository candidates;
	private CirconscriptionsRepository circonscriptions;
	private DataNormalizer normalizer;

	@Autowired
	public CirconscriptionController(
			CandidatesRepository candidates, 
			CirconscriptionsRepository circonscriptions,
			DataNormalizer normalizer
			) {
		this.candidates = candidates;
		this.circonscriptions = circonscriptions;
		this.normalizer = normalizer;
	}

	@RequestMapping(value="/potential", method= RequestMethod.GET)
	public @ResponseBody Map<String, List<Candidate>> allCircoWithPotentialCandidates() {
		return circonscriptions.all()
			.map(Circonscription::getId)
			.collect(Collectors.toMap(Function.identity(), this::listPotentialCandidates));
	}
	
	@RequestMapping(value="/{circo}/potential", method= RequestMethod.GET)
	public List<Candidate> listPotentialCandidates(String circo) {
		return Stream.concat(
				fromCirconscriptions(circo),
				fromCandidates(circo)
			)
			.filter(c -> c != null)
			.distinct()
			.collect(Collectors.toList());
	}
	
	public Stream<Candidate> fromCirconscriptions(String circo) {
		Circonscription circonscription = circonscriptions.getById(circo);
		Candidate candidate = candidates.getByName(circonscription.getCandidate());
		Candidate substitute = candidates.getByName(circonscription.getSubstitute());
		return Arrays.asList(candidate, substitute).stream();
	}
	
	public Stream<Candidate> fromCandidates(String circo) {
		return candidates.all()
				.filter(c -> normalizer.normalize(c.getCircos()).contains(circo));
	}
}

package fr.partipirate.legislatives2017.candidatures;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class App {

    public static void main(String...args) throws Exception {
        new SpringApplicationBuilder()
                .sources(App.class)
                .run(args);
    }

}

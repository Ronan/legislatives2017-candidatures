package fr.partipirate.legislatives2017.candidatures.repositories;

import java.io.IOException;

import org.springframework.stereotype.Service;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;

@Service
public class CandidatesRepository extends RemoteCsvRepository<Candidate> {

    public CandidatesRepository() throws IOException {
        super("https://framacalc.org/rdseG4rS22.1.csv", "candids.csv");
    }

    public Candidate getById(long id) {
        return getUnique(c -> c.getId().equals(id));
    }

    public Candidate getByName(String name) {
        return getUnique(c -> c.getName().equals(name));
    }

    public long getMaxId() {
        return this.all()
                .mapToLong(Candidate::getId)
                .max()
                .orElse(0);
    }

    protected Candidate buildElementFrom(String line) {
        return new Candidate(line);
    }
}

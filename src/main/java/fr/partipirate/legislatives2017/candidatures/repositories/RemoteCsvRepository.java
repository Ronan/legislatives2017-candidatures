package fr.partipirate.legislatives2017.candidatures.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class RemoteCsvRepository<T> {

    private List<T> data;

    public RemoteCsvRepository(String url, String resource) throws IOException {
    	try {
	        this.data = curl(url);
    	} catch (Exception e) {
    		this.data = fallback(resource);
    	}
    }

    protected abstract T buildElementFrom(String line);

    public long count() {
        return this.data.size();
    }

    public Stream<T> all() {
        return this.data.stream();
    }

    public T getUnique(Predicate<T> filter) {
        return this.all()
                .filter(filter::test)
                .findAny()
                .orElse(null);
    }

	private List<T> fallback(String resource) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(resource), Charset.forName("utf-8")))) {
			return reader.lines().skip(1)
		            .map(this::buildElementFrom)
		            .collect(Collectors.toList());
		}
	}

	private List<T> curl(String url) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("curl", url);
		Process p = pb.start();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
		    return br.lines().skip(1)
		            .map(this::buildElementFrom)
		            .collect(Collectors.toList());
		}
	}
}

package fr.partipirate.legislatives2017.candidatures.repositories;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;

public class CandidatesRepositoryShould {

	private static CandidatesRepository repository;
	
	@BeforeClass
	public static void setup() throws IOException {
		repository = new CandidatesRepository();
	}
	
	@Test
	public void retreive_all_candidates() {
		Assert.assertEquals(138, repository.count());
	}

	@Test
	public void retreive_a_candidate_with_id_130() {
		Candidate expected = new Candidate("130,Nicolas Bedel,male");
		Assert.assertEquals(expected, repository.getById(130));
	}
	
	@Test
	public void retreive_a_candidate_with_id_138() {
		Candidate expected = new Candidate("138,Adeline Bartoletti,female,1,1,1,,,,#Circo6901");
		Assert.assertEquals(expected, repository.getById(138));
	}
	
	@Test
	public void retreive_a_candidate_with_id_6() {
		Candidate actual = repository.getById(6);
		Candidate expected = new Candidate("6,\"Raphael Isla\",male,1,1,,Occitanie,oui,\"Raphael Isla\",\"#Circo6501, #Circo6502\"");
		Assert.assertEquals(expected, actual);
		Assert.assertEquals("#Circo6501, #Circo6502", actual.getCircos());
	}

	@Test
	public void retreive_a_candidate_with_name_Ronan_Le_Roy() {
		Candidate candidate = repository.getByName("Ronan Le Roy");
		Assert.assertNotNull(candidate);
	}
}

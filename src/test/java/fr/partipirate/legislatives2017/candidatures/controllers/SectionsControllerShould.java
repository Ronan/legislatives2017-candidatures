package fr.partipirate.legislatives2017.candidatures.controllers;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;
import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;

public class SectionsControllerShould {
	
	private static SectionsController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		controller = new SectionsController(new CandidatesRepository());
	}
	

	@Test
	public void list_all_sections() {
		List<String> sections = controller.all();
		System.out.println("=== SECTIONS ===");
		sections.stream()
			.forEach(System.out::println);
		System.out.println("================");
		Assert.assertEquals(5, sections.size());
		Assert.assertTrue(sections.contains("IdF"));
	}
	
	@Test
	public void list_all_candidates_without_tuteur_by_sections() {
		List<Candidate> candidates = controller.withoutTutor("IdF");
		Assert.assertEquals(2, candidates.size());
		Assert.assertEquals(0l, candidates.stream().filter(c -> "Ronan Le Roy".equals(c.getName())).count());
		Assert.assertEquals(1l, candidates.stream().filter(c -> "Thomas Watanabe-Vermorel".equals(c.getName())).count());
	}
}

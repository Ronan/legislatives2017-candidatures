package fr.partipirate.legislatives2017.candidatures.controllers;

import java.io.IOException;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;

public class StatisticsControllerShould {

	private static StatisticsController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		controller = new StatisticsController(new CandidatesRepository());
	}
	
	@Test
	public void provide_repository_size() {
		Map<String, String> stats = controller.stats();
		Assert.assertEquals("138", stats.get("repository.size"));
	}
	
	@Test
	public void provide_repository_max_id() {
		Map<String, String> stats = controller.stats();
		Assert.assertEquals("161", stats.get("repository.maxid"));
	}

}

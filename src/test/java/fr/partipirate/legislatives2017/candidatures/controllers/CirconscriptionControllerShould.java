package fr.partipirate.legislatives2017.candidatures.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;
import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.candidatures.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.candidatures.services.DataNormalizer;

public class CirconscriptionControllerShould {

	private static CirconscriptionController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circonscriptions = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circonscriptions);
		controller = new CirconscriptionController(
				new CandidatesRepository(),
				circonscriptions,
				normalizer
				);
	}
	
	@Test
	public void list_circo_with_potential_candidates() {
		Map<String, List<Candidate>> potential = controller.allCircoWithPotentialCandidates();
		
		Assert.assertEquals(577, potential.size());
	}

	@Test
	public void list_potential_candidates_for_circos() {
		Assert.assertEquals(1, controller.listPotentialCandidates("075-01").stream().map(Candidate::getName).filter("Ronan Le Roy"::equals).count());
		Assert.assertEquals(0, controller.listPotentialCandidates("075-02").stream().map(Candidate::getName).filter("Ronan Le Roy"::equals).count());
		Assert.assertEquals(1, controller.listPotentialCandidates("082-02").stream().map(Candidate::getName).filter("Cédric Levieux"::equals).count());
	}
}

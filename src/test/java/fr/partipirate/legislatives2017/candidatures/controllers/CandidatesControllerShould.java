package fr.partipirate.legislatives2017.candidatures.controllers;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.candidatures.beans.Candidate;
import fr.partipirate.legislatives2017.candidatures.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.candidatures.repositories.CirconscriptionsRepository;

public class CandidatesControllerShould {
	
	private static CandidatesController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		controller = new CandidatesController(new CandidatesRepository(), new CirconscriptionsRepository());
	}
	
	@Test
	public void provide_candidates_on_circo_not_registered() {
		List<Candidate> notRegistered = controller.notRegistered();
		Candidate expected = new Candidate(",Aurélie,,0,1,0,,,,#075-18");
		Assert.assertTrue(notRegistered.contains(expected));
	}

	@Test
	public void not_provide_registered_candidates() {
		List<Candidate> notRegistered = controller.notRegistered();
		long count = notRegistered.stream()
				.filter(candidate -> "Ronan Le Roy".equals(candidate.getName()))
				.count();
		Assert.assertEquals(0l, count);
	}

	@Test
	public void list_not_registered_candidates() {
		List<Candidate> notRegistered = controller.notRegistered();
		System.out.println("=== NOT REGISTERED ===");
		notRegistered.stream()
			.map(c -> c.getCircos() + " - " + c.getName())
			.forEach(System.out::println);
		System.out.println("======================");
		Assert.assertEquals(7l, notRegistered.size());
	}
	
	@Test
	public void list_candidates_without_section() {
		List<Candidate> withoutSection = controller.withoutSection();
		System.out.println("=== NO SECTION ===");
		withoutSection.stream()
			.map(c -> c.getName() + " - " + c.getCircos())
			.forEach(System.out::println);
		System.out.println("==================");
		Assert.assertEquals(102l, withoutSection.size());
	}
}
